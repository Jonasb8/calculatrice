import { useState, useCallback } from "react";
import ButtonWrapper from './ButtonWrapper';
import Screen from './Screen';
import './Calculator.css';
import { calculate } from '../Helpers/calculator.js';

const Calculator = () => {
    const [displayedValue, setDisplayedValue] = useState(0);
    const [selectedValuesToProcess, setSelectedValuesToProcess] = useState([]);

    function resetWithValue(value) {
        setDisplayedValue(value);
        setSelectedValuesToProcess(
            selectedValuesToProcess
                .splice(0, selectedValuesToProcess.length)
        );
    }

    function calculateAndReset(selectedValuesToProcess, param = null) {
        let result = calculate(selectedValuesToProcess, param);
        resetWithValue(result);
        setSelectedValuesToProcess(selectedValuesToProcess.push(result));
    }

    function calculateAndDisplayResult(input, triggerCalculation) {
        setSelectedValuesToProcess(selectedValuesToProcess.push(input));

        if (triggerCalculation === true) {
            calculateAndReset(selectedValuesToProcess);
        } else {
            setDisplayedValue(selectedValuesToProcess);
        }
    }

    const callback = useCallback((input, triggerCalculation) => {
        switch (input) {
            case 'C':
                resetWithValue(0);
                break;
            case 'log':
                calculateAndReset(selectedValuesToProcess, 'log');
                break;
            case 'exp':
                calculateAndReset(selectedValuesToProcess, 'exp');
                break;
            default:
                calculateAndDisplayResult(input, triggerCalculation);
        }
    }, []);

    return (
        <div className='wrapper'>
            <Screen displayedValue={displayedValue}/>
            <ButtonWrapper
                calculatorCallback={callback}
            />
        </div>
    );
};

export default Calculator;
