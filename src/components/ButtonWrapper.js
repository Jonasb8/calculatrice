import { useCallback, useEffect } from 'react';
import './ButtonWrapper.css';
import Button from './Button';

const btnValues = [
    ['C', 'log', 'exp', '/'],
    [7, 8, 9, 'x'],
    [4, 5, 6, '-'],
    [1, 2, 3, '+'],
    [0, '.', '='],
];

const acceptedNumericValuesFromKeys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
const acceptedMathsValuesFromKeys = ['/', '+', '-', 'x', '.'];

const ButtonWrapper = ({ calculatorCallback }) => {

    function handleFormatting(selectedButton) {
        let triggerCalculation = selectedButton === '=' ? true : false;
        calculatorCallback(selectedButton, triggerCalculation);
    }

    const handleKeyPress = useCallback((event) => {
        if (acceptedNumericValuesFromKeys.includes(event.key)) {
            calculatorCallback(parseInt(event.key), false);
        }
        if (acceptedMathsValuesFromKeys.includes(event.key)) {
            calculatorCallback(event.key, false);
        }
        if (event.key === 'Enter') {
            calculatorCallback(null, true);
        }
    }, []);

    useEffect(() => {
        // attach the event listener
        document.addEventListener('keydown', handleKeyPress);

        // remove the event listener
        return () => {
          document.removeEventListener('keydown', handleKeyPress);
        };
    }, [handleKeyPress]);

    return (
        <div className='ButtonWrapper'>
            {
                btnValues.flat().map((selectedButton, i) => {
                    return (
                        <Button
                            key={i}
                            className={selectedButton === '=' ? 'equals' : ''}
                            value={selectedButton}
                            onClick={() => handleFormatting(selectedButton)}
                        />
                    );
                })
            }
        </div>
    );
};

export default ButtonWrapper;
