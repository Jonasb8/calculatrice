import './Screen.css';

const Screen = ({ displayedValue }) => {
    return (
        <div className="screen">{displayedValue}</div>
    );
};

export default Screen;
