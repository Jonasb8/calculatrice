import { create, all } from 'mathjs';

const config = {};
const math = create(all, config);

export function calculate(selectedValuesToProcess, option = null) {
    let expression = selectedValuesToProcess
        .toString()
        .replace (/x/g, "*")
        .replace (/,|=/g, "");
    let result = math.evaluate(expression);

    if (option === 'log') {
        return math.round(math.log10(result), 2);
    }

    if (option === 'exp') {
        return math.round(math.exp(result), 2);
    }

    return math.round(result, 2);
}
